<?php

/**
 * @file
 * Definition of DrupalGeofieldPluginStorageBase.
 */

// @todo - Uncomment in D8
//namespace Drupal\geofield\Plugin\Storage;

module_load_include('inc', 'geofield', 'lib/Drupal/geofield/Plugin/Storage/Default');

class DrupalGeofieldPluginStorageRemote extends DrupalGeofieldPluginStorageDefault {
  public function getRemoteStorageInstance() {
    return $this;
  }

  public function getFieldSchema() {
    $schema = parent::getFieldSchema();

    $schema['columns']['grd_id'] = array(
      'description' => 'The remote geo database table id.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => FALSE,
      'default' => NULL,
    );
    $schema['columns']['grd_status'] = array(
      'description' => 'The remote geo database table status.',
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    );

    return $schema;
  }

  public function fieldSettingsForm($field, $instance, $has_data) {
    $form = parent::fieldSettingsForm($field, $instance, $has_data);
    $settings = $field['settings'];
    $form['grd-table-name'] = array(
      '#type' => 'value',
      '#value' => isset($settings['grd-table-name']) ? $settings['grd-table-name'] : '',
    );
    $form['grd-table-status'] = array(
      '#type' => 'value',
      '#value' => isset($settings['grd-table-status']) ? $settings['grd-table-status'] : GRD_TABLE_NOT_CREATED,
    );
    return $form;
  }

  function fieldInsert($entity_type, $entity, $field, $instance, $langcode, &$items) {
    // Create the table if not created
    if (!$this->isTableCreated($field)) {
      if ($this->createRemoteTable($field) !== NULL) {
        $this->saveField($field);
      }
    }
    geophp_load();
    foreach ($items as &$item) {
      $item['grd_id'] = NULL;
      $item['grd_status'] = GRD_NOT_SYNC;
      // Only try to sync if the table is created
      if ($this->isTableCreated($field)) {
        if (!$this->fieldIsEmpty($item, $field)) {
          $geometry = geoPHP::load($item['geom'], 'wkb');
          $wkt = $geometry->out('wkt');
          // Only insert rows if not field not empty
          if (($grd_id = $this->getRemoteStorageInstance()->insertRow($field['settings']['grd-table-name'], $wkt)) != NULL) {
            $item['grd_id'] = $grd_id;
            $item['grd_status'] = GRD_SYNC;
          }
        }
        else {
          // If is empty is always sync
          $item['grd_status'] = GRD_SYNC;
        }
      }
    }
    parent::fieldInsert($entity_type, $entity, $field, $instance, $langcode, $items);
  }

  function fieldPresave($entity_type, $entity, $field, $instance, $langcode, &$items) {
    parent::fieldPresave($entity_type, $entity, $field, $instance, $langcode, $items);
    if (isset($entity->original)) {
      foreach ($items as $position => &$item) {
        $grd_id = isset($entity->original->{$field['field_name']}[$langcode][$position]['grd_id']) ?
          $entity->original->{$field['field_name']}[$langcode][$position]['grd_id'] :
          NULL;
        $item['grd_id'] = $grd_id;
        $grd_status = isset($entity->original->{$field['field_name']}[$langcode][$position]['grd_status']) ?
          $entity->original->{$field['field_name']}[$langcode][$position]['grd_status'] :
          GRD_NOT_SYNC;
        $item['grd_status'] = $grd_status;
      }
    }
  }

  function fieldUpdate($entity_type, $entity, $field, $instance, $langcode, &$items) {
    // Create the table if not created
    if (!$this->isTableCreated($field)) {
      if ($this->createRemoteTable($field) !== NULL) {
        $this->saveField($field);
      }
    }
    geophp_load();
    foreach ($items as &$item) {
      // Not empty field
      if (!$this->fieldIsEmpty($item, $field)) {
        $item['grd_status'] = GRD_NOT_SYNC;
        // If the table is not created GRD_ID is NULL and we do not insert
        // or update rows
        if ($this->isTableCreated($field)) {
          $geometry = geoPHP::load($item['geom'], 'wkb');
          $wkt = $geometry->out('wkt');
          if ($item['grd_id'] == NULL) {
            if (($grd_id = $this->getRemoteStorageInstance()->insertRow($field['settings']['grd-table-name'], $wkt)) !== NULL) {
              $item['grd_id'] = $grd_id;
              $item['grd_status'] = GRD_SYNC;
            }
          }
          else {
            if ($this->getRemoteStorageInstance()->updateRow($field['settings']['grd-table-name'], $item['grd_id'], $wkt) !== NULL) {
              $item['grd_status'] = GRD_SYNC;
            }
          }
        }
      }
      // Empty field
      else {
        // If the table is not created or grd_id is NULL there is no need to
        // delete de row
        if ($this->isTableCreated($field) && ($item['grd_id'] != NULL)) {
          $this->getRemoteStorageInstance()->deleteRow($field['settings']['grd-table-name'], $item['grd_id']);
          $item['grd_id'] = NULL;
          $item['grd_status'] = GRD_SYNC;
        }
      }
    }
    parent::fieldUpdate($entity_type, $entity, $field, $instance, $langcode, $items);
  }

  function fieldDelete($entity_type, $entity, $field, $instance, $langcode, &$items) {
    if ($this->isTableCreated($field)) {
      foreach ($items as &$item) {
        if ($item['grd_id'] != NULL) {
          $this->getRemoteStorageInstance()->deleteRow($field['settings']['grd-table-name'], $item['grd_id']);
        }
      }
    }
    parent::fieldDelete($entity_type, $entity, $field, $instance, $langcode, $items);
  }

  function fieldPurgeField($field) {
    if ($this->isTableCreated($field)){
      $this->getRemoteStorageInstance()->deleteTable($field['settings']['grd-table-name']);
    }
    parent::fieldPurgeField($field);
  }

  public function fieldUpdateField($field, $prior_field, $has_data) {
    if (!$this->isTableCreated($field)){
      if ($this->createRemoteTable($field) !== NULL) {
        $this->saveField($field);
      }
    }
    parent::fieldUpdateField($field, $prior_field, $has_data);
  }

  /**
   * Given an status and the sync flag return the status string.
   */
  public function statusToString($status, $sync = NULL) {
    $ret = '';
    if ($sync || ($sync === NULL)) {
      if ($sync !== NULL) {
        $ret = t('Sync enabled');
        $ret .= ' - ';
      }
      switch ($status) {
        case GRD_TABLE_NOT_CREATED:
          $ret .= t('Table not created');
          break;
        case GRD_NOT_SYNC:
          $ret .= t('Out of sync');
          break;
        case GRD_SYNC:
          $ret .= t('Syncronized');
          break;
      }
    }
    else {
      $ret = t('Sync disabled');
    }
    return $ret;
  }

  /**
   * Creates the remote field and changes field in success.
   */
  protected function createRemoteTable(&$field) {
    $remote_instance = $this->getRemoteStorageInstance();
    $table_name = $remote_instance->getTableName($field['field_name']);
    if (($ret = $remote_instance->createTable($table_name)) === NULL) {
      return NULL;
    }
    $field['settings']['grd-table-name']  = $ret;
    $field['settings']['grd-table-status'] = GRD_NOT_SYNC;
    return 0;
  }

  /**
   * Saves the field to the database;
   */
  protected function saveField($field) {
    // The serialized 'data' column contains everything from $field that does not
    // have its own column and is not automatically populated when the field is
    // read.
    $data = $field;
    unset($data['columns'], $data['field_name'], $data['type'], $data['locked'], $data['module'], $data['cardinality'], $data['active'], $data['deleted']);
    // Additionally, do not save the 'bundles' property populated by
    // field_info_field().
    unset($data['bundles']);

    $field['data'] = $data;

    // Store the field and create the id.
    $primary_key = array('id');
    drupal_write_record('field_config', $field, $primary_key);

    // Clear caches
    field_cache_clear(TRUE);
  }

  protected function isTableCreated($field) {
    return isset($field['settings']['grd-table-status']) && ($field['settings']['grd-table-status'] != GRD_TABLE_NOT_CREATED);
  }
}
