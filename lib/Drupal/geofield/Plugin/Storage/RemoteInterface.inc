<?php

/**
 * @file
 * Definition of DrupalGeofieldPluginStorageRemoteInterface.
 */

// @todo - Uncomment in D8
//namespace Drupal\geofield\Plugin\Storage;

interface DrupalGeofieldPluginStorageRemoteInterface {
  public function getTableName($field_name);
  public function createTable($name);
  public function deleteTable($name);
  public function insertRow($table, $wkt);
  public function deleteRow($table, $id);
  public function updateRow($table, $id, $wkt);
}