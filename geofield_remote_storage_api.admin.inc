<?php

function geofield_remote_storage_api_sync_list() {
  $rows = array();
  $fields = field_read_fields(array('type' => 'geofield'));
  $header = array(t('Field name'), t('Syncronized'), t('Remote table name'), t('Used in'), t('Op'));
  $bundles = field_info_bundles();
  $entities = entity_get_info();

  foreach ($fields as $field) {
    $instances = _geo_database_get_instances_by_field($field['field_name']);
    $used_in = array();
    foreach ($instances as $instance) {
      $entity_label = $entities[$instance['entity_type']]['label'];
      $bundle_label = $bundles[$instance['entity_type']][$instance['bundle']]['label'];
      if ($entity_label == $bundle_label) {
        $used_in[] = $entity_label;
      }
      else {
        $used_in[] = $entity_label . ': ' . $bundle_label;
      }
    }
    $status = geo_database_status_to_string($field['settings']['gd-table-status'], $field['settings']['gd-sync']);

    $links = array();
    if ($field['settings']['gd-sync']) {
      $links['sync'] = array(
        'title' => t('Sync'),
        'href' => 'admin/config/system/geo/sync/' . $field['field_name'],
      );
    }
    $rows[] = array(
      $field['field_name'],
      $status,
      $field['settings']['gd-table-name'],
      implode(', ', $used_in),
      theme('links', array('links' => $links)),
    );
  }

  if (empty($rows)) {
    return t('No fields have been defined yet.');
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

function geofield_remote_storage_api_field_sync_form($form, &$form_state, $field) {
  $options = array();
  $bundles = field_info_bundles();
  $entities = entity_get_info();

  $field_table = _field_sql_storage_tablename($field);
  $result = db_select($field_table, 'f')
     ->fields('f')
     ->execute();

  while ($record = $result->fetchAssoc()) {
    $entities_array = entity_load($record['entity_type'], array($record['entity_id']));
    $entity = current($entities_array);
    $uri = entity_uri($record['entity_type'], $entity);
    $status = _field_sql_storage_columnname($field['field_name'], 'gd_status');
    $options[$record['entity_type'] . '-' . $record['entity_id']] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => entity_label($record['entity_type'], $entity),
          '#href' => $uri['path'],
        ),
      ),
      'type' => $entities[$record['entity_type']]['label'],
      'bundle' => $bundles[$record['entity_type']][$record['bundle']]['label'],
      'status' => geo_database_status_to_string($record[$status]),
    );
  }

  $header = array(
    'title' => t('Title'),
    'type' => t('Type'),
    'bundle' => t('Bundle'),
    'status' => t('Status'),
  );

  $form['entities'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No entities available.'),
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
